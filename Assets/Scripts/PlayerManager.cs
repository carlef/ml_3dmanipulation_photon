﻿using UnityEngine;
using UnityEngine.EventSystems;

using System.Collections;
using Photon.Pun;
using UnityEngine.UI;

using UnityEngine.XR.MagicLeap;

namespace com.defaultcompany.photontest
{
    /// <summary>
    /// Player manager.
    /// Handles controller input.
    /// </summary>
    public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
    {
        #region Private Fields
        //[SerializeField]
        //GameObject _camera = null;
        //[SerializeField]
        //GameObject _pointer = null;

        #endregion

        #region Public Fields 

        [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
        public static GameObject LocalPlayerInstance;

        #endregion

        #region IPunObservable implementation

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // We own this player: send the others our data
                //stream.SendNext(_bumper);
                //stream.SendNext(_bumper_time);
            }
            else
            {
                // Network player, receive data
                //_bumper = (bool)stream.ReceiveNext();
                //_bumper_time = (int)stream.ReceiveNext();
            }
        }


        #endregion

        #region MonoBehaviour CallBacks

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #Important
            // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
            if (photonView.IsMine)
            {
                LocalPlayerInstance = this.gameObject;
            }
            // #Critical
            // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
            //DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
           // TransmissionObject CameraTransmission = Transmission.Spawn("DefaultTransmissionObject",
           //_camera.transform.position,
           //_camera.transform.rotation,
           //_camera.transform.localScale);
           // CameraTransmission.motionSource = _camera.transform;

           // TransmissionObject PointerTransmission = Transmission.Spawn("DefaultTransmissionObject",
           //_pointer.transform.position,
           //_pointer.transform.rotation,
           //_pointer.transform.localScale);
           // PointerTransmission.motionSource = _pointer.transform;
        }

        public override void OnDisable()
        {
            // Always call the base to remove callbacks
            base.OnDisable();
        }

        void OnDestroy()
        {
            if (photonView.IsMine == false && PhotonNetwork.IsConnected)
            {
                return;
            }
        }

        #endregion
    }
}