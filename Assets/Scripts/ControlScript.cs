﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using Photon.Pun;
using UnityEngine.UI;

public class ControlScript : MonoBehaviour
{
    #region IPunObservable implementation


    #endregion

    #region Public Fields 

    public GameObject _pointer;

    #endregion

    private MLInputController _controller;
    [SerializeField]
    private GameObject _control_panel = null;
    private UnityEngine.UI.Button UIButton;
    private Button[] _buttons;
    private int _button_index;
    //private int _trigger_time = 0;
    private const float _triggerThreshold = 0.2f;
    private bool _trigger = false;
    private bool _trigger_prev = false;
    private bool _touch = false;
    private bool _touch_prev = false;

    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        MLInput.OnControllerButtonDown += OnButtonDown;
        MLInput.OnControllerButtonUp += OnButtonUp;

        _controller = MLInput.GetController(MLInput.Hand.Left);

        _buttons = _control_panel.GetComponentsInChildren<Button>();
        _button_index = 0;
        UIButton = _buttons[_button_index];
        _buttons[_button_index].Select();
    }

    void OnDestroy()
    {
        MLInput.OnControllerButtonDown -= OnButtonDown;
        MLInput.OnControllerButtonUp -= OnButtonUp;
        MLInput.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        CheckControl();
        MovePointer();
        _buttons[_button_index % _buttons.Length].Select();

        if (_trigger_prev && !_trigger)
        {
            if(UIButton != null)
            {
                UIButton.onClick.Invoke();
            }
            else if (_pointer != null)
            {
                //do something
            }
        }
        if(_touch_prev && !_touch)
        {
            _button_index++;
            UIButton = _buttons[_button_index % _buttons.Length];
        }
    }

    void CheckControl()
    {
        if (_controller.TriggerValue > 0.2f) { 
            _trigger_prev = _trigger;
            _trigger = true;
        }
        else
        {
            _trigger_prev = _trigger;
            _trigger = false;
        }
        if (_controller.Touch1PosAndForce.z > 0.01f)
        {
            _touch_prev = _touch;
            _touch = true;
            //    float X = _controller.Touch1PosAndForce.x;
            //    float Y = _controller.Touch1PosAndForce.y;
            //    Vector3 forward = Vector3.Normalize(Vector3.ProjectOnPlane(transform.forward, Vector3.up));
            //    Vector3 right = Vector3.Normalize(Vector3.ProjectOnPlane(transform.right, Vector3.up));
            //    Vector3 force = Vector3.Normalize((X * right) + (Y * forward));
            //    _cube.transform.position += force * Time.deltaTime * _moveSpeed;
        }
        if (_controller.Touch1PosAndForce.z < 0.01f)
        {
            _touch_prev = _touch;
            _touch = false;
        }
    }

    void MovePointer()
    {
        if(_pointer != null)
        {
            _pointer.transform.position = _controller.Position;
            _pointer.transform.rotation = _controller.Orientation;
        }
    }

    void OnButtonDown(byte controller_id, MLInputControllerButton button)
    {
        if ((button == MLInputControllerButton.Bumper))
        {
        }
    }

    void OnButtonUp(byte controller_id, MLInputControllerButton button)
    {
        if (button == MLInputControllerButton.HomeTap)
        {

        }
        if (button == MLInputControllerButton.Bumper)
        {
            if (UIButton != null)
            {
                UIButton.onClick.Invoke();
            }
            else if (_pointer != null)
            {
                //do something
            }
        }

    }
}
