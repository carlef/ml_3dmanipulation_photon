﻿using UnityEngine;
using Photon.Pun;

public class ObjectSpawner : MonoBehaviourPun
{

    [SerializeField]
    private SelectableObject _spawnObject = null;
    //private SelectableObject _spawnedObject;
    SelectableObject _spawnedObject;

    [SerializeField]
    private Transform _parent = null;

    [SerializeField]
    private float _scale = 0;
    [SerializeField]
    private float _yOffset = 0;
    //private double _rotateSpeed;

    private int _objectCount = 0;
    private string prefab_name;

    // Start is called before the first frame update
    void Start()
    {
        //Mesh mesh = _spawnObject.GetComponent<MeshFilter>().sharedMesh;
        //Mesh mesh2 = Instantiate(mesh);
        //_previewObject.GetComponent<MeshFilter>().sharedMesh = mesh;
        _scale = _scale * transform.localScale.x;
        prefab_name = _spawnObject.name;
        if (PhotonNetwork.IsMasterClient)
        {
            SpawnObject();
            InvokeRepeating("Rotate", 2.0f, 0.1f);
        }
    }

    void SpawnObject()
    {
        _spawnedObject = PhotonNetwork.Instantiate(prefab_name,
            transform.position + new Vector3(0, _yOffset, 0),
            transform.rotation)
            .GetComponent<SelectableObject>();

        Debug.Log(photonView);
        _spawnedObject.SetPrefabName(prefab_name);
        photonView.RPC("RPC_SetPrefabName", RpcTarget.OthersBuffered, _spawnedObject.gameObject.GetPhotonView().ViewID);
        //_spawnedObject.transform.parent = _parent;
        _spawnedObject.transform.localScale = new Vector3(_scale, _scale, _scale);
        _spawnedObject.name = prefab_name + "(" + _objectCount + ")";
        _objectCount++;

        //TransmissionObject spawn = Transmission.Spawn("DefaultTransmissionObject",
        //    _spawnedObject.transform.position,
        //    _spawnedObject.transform.rotation,
        //    _spawnedObject.transform.localScale);
        //spawn.motionSource = _spawnedObject.transform;
    }

    [PunRPC]
    void RPC_SetPrefabName(int object_id)
    {
        PhotonView.Find(object_id).GetComponent<SelectableObject>().SetPrefabName(prefab_name);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Rotate()
    {
        _spawnedObject.transform.rotation = _spawnedObject.transform.rotation * Quaternion.Euler(0, 1, 0);
    }

    private void OnTriggerExit(Collider other) 
    {
        //Debug.Log("collison exit: " + other.gameObject);
        //Debug.Log("_spawnedObject: " + _spawnedObject.gameObject);
        if(PhotonNetwork.IsMasterClient){
            if (ReferenceEquals(other.gameObject, _spawnedObject.gameObject))
            {
                SpawnObject();
            }
        }
        
    }
}
