﻿Shader "Interface3/SurfaceOutline"
{
	Properties
	{
		_Color("Color",Color) = (1,1,1,1)
		_MainTex("Albedo" , 2D) = "white"{}
		[NoScaleOffset]_BumpMap("Normal map" , 2D) = "bump"{}
		_BumpScale("Normal scale", Range(0,2)) = 1
		_Smoothness("Smoothness",Range(0,1)) = 0.5
		_Metallic("Metallic",Range(0,1)) = 0.0
		_OutlineThickness("Outline thickness", Range(0,1)) = 0.5
		_OutlineColor("Outline color", Color) = (0,0,0,1)
	}
		SubShader
		{
			Tags{ "RenderType" = "Opaque"}
			LOD 200

			Cull back

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _BumpMap;

			struct Input
			{
				float2 uv_MainTex;
			};

			float _Smoothness;
			float _Metallic;
			float _BumpScale;
			float4 _Color;

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				float4 c = tex2D(_MainTex, IN.uv_MainTex)*_Color;
				o.Albedo = c.rgb;
				float4 normal = tex2D(_BumpMap, IN.uv_MainTex);
				o.Normal = UnpackScaleNormal(normal, _BumpScale);
				o.Metallic = _Metallic;
				o.Smoothness = _Smoothness;
			}

		ENDCG

			Cull front

			CGPROGRAM
			#pragma surface surf Standard fullforwardshadows vertex:vert noambient
			#pragma target 3.0

			sampler2D _MainTex;

		struct Input
		{
			float2 uv_MainTex;
		};

		float _Smoothness;
		float _Metallic;
		float _BumpScale;
		float4 _Color;
		float _OutlineThickness;
		float4 _OutlineColor;

		void vert(inout appdata_full v)
		{
			float3 worldPosition = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1)).xyz;
			float worldDistance = length(_WorldSpaceCameraPos - worldPosition);
			//pour supprimer l'effet où les lignes de l'outline restent de la même épaisseur sans prendre compte de la distance de la caméra, retirer les commentaires et le point virugule d ela ligne du dessous

			v.vertex.xyz += v.normal * _OutlineThickness;// *worldDistance*0.025;
		}

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Emission = _OutlineColor;
		}

		ENDCG
		}
			FallBack "Diffuse"
}
