﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using com.defaultcompany.photontest;
using UnityEngine.UI;
using UnityEngine.XR.MagicLeap;
using UnityEngine.EventSystems;
using System;

public class GameMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject _selection_name = null;
    [SerializeField]
    private GameObject _tool_name = null;
    //[SerializeField]
    //private GameObject _reset_button = null;
    //[SerializeField]
    //private GameObject _delete_button = null;
    //[SerializeField]
    //private GameObject _leave_button = null;
    //[SerializeField]
    //private GameObject _button_layout = null;
    //[SerializeField]
    //private GameObject _duplicate_button = null;

    [SerializeField, Tooltip("Objects used to control selection")]
    private Selector[] _selectors = null;

    [SerializeField, Tooltip("Array used for navigation of buttons")]
    private Button[] _left_buttons = null;
    [SerializeField, Tooltip("Array used for navigation of buttons")]
    private Button[] _right_buttons = null;
    [SerializeField, Tooltip("Array used for navigation of buttons")]
    private Button[] _bottom_buttons = null;

    private GameObject _selection = null;

    private string _tool = null;

    private bool _button_layout_visible = true;

    //private int _button_index = 0;
    //private Button[] _buttons;
    private Button _UIButton;
    private List<Button[]> _buttons;

    private void Awake()
    {
        //_leave_button.transform.SetAsLastSibling();
        //_buttons = transform.GetChild(0).GetComponentsInChildren<Button>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //if (!photonView.IsMine)
        //{
        //    ToggleButtonLayout();
        //}
        //_UIButton = _buttons[_button_index];
        _buttons = new List<Button[]>() { _left_buttons, _right_buttons, _bottom_buttons};

        HighlightFirst();
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
        if (active)
        {
            HighlightFirst();
        }
    }

    public void HighlightFirst()
    {
        HighlightButton(_left_buttons[0]);
    }

    public void ResetSelectedTransforms()
    {
        if(_selection != null)
        {
            _selection.transform.localScale = new Vector3(1, 1, 1);
            _selection.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
    }

    public void DeleteSelected()
    {
        if(_selection != null)
        {
            foreach (Selector s in _selectors)
            {
                s.Delete(_selection);
            }
            PhotonNetwork.Destroy(_selection);
        }
    }

    public void DuplicateSelected()
    {
        if(_selection != null)
        {
            SelectableObject duplicate = 
                PhotonNetwork.Instantiate(_selection.GetComponent<SelectableObject>().GetPrefabName(), 
                _selection.transform.position + new Vector3(0, 0, .05f), 
                _selection.transform.rotation).GetComponent<SelectableObject>();
            duplicate.SetPrefabName(_selection.GetComponent<SelectableObject>().GetPrefabName());
            duplicate.transform.parent = _selection.transform.parent;
            duplicate.transform.localScale = _selection.transform.localScale;
            duplicate.name = _selection.name + " (dup)";
        }
    }

    public void SetSelection(GameObject selection)
    {
        _selection = selection;
    }

    public void SetTool(string toolname)
    {
        _tool = toolname;
    }

    public void ToggleButtonLayout()
    {
        _button_layout_visible = !_button_layout_visible;
        //_button_layout.SetActive(_button_layout_visible);
    }

    public void LeaveRoom()
    {
        Destroy(PlayerManager.LocalPlayerInstance);
        //PhotonNetwork.Destroy(PhotonView);
        PhotonNetwork.LeaveRoom();
    }

    public void HighlightButton(Button button)
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
        //_button_index = System.Array.IndexOf(_buttons, button);
        //_UIButton = _buttons[_button_index];
        _UIButton = button;
        //Debug.Log("Highlighting by object reference: " + _UIButton);
        _UIButton.Select();
    }

    //public void HighlightButton(int button)
    //{
    //   EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
    //    _button_index = button;
    //    _UIButton = _buttons[_button_index % _buttons.Length];
    //    _UIButton.Select();
    //    Debug.Log("Highlighting by index: " + _UIButton);
    //}

    public void PushButton()
    {
        Debug.Log("Pushing: " + _UIButton);
        _UIButton.onClick.Invoke();
    }

    //public void Next()
    //{
    //    _button_index++;
    //    HighlightButton(_button_index);
    //}

    //public void Previous()
    //{
    //    _button_index = --_button_index + _buttons.Length;
    //    HighlightButton(_button_index);
    //}

    public void TouchOff(float x, float y)
    {
        Debug.Log("Touch Menu");
        Button[] locationInUI = _left_buttons;
        int button_index = 0;
        float touchLimit = 0.3f;
        foreach(Button[] buttonlist in _buttons)
        {
            if (Array.IndexOf(buttonlist, _UIButton) > -1)
            {
                Debug.Log("Location exists");
                button_index = Array.IndexOf(buttonlist, _UIButton);
                locationInUI = buttonlist;
            }
        }

       
        if(Math.Abs(y) > touchLimit)
        {
            if ((locationInUI != _bottom_buttons))
            {
                if ((button_index == 0 && y > 0) || (button_index == locationInUI.Length - 1 && y < 0))
                {
                    Debug.Log("At top or end of left/right, going to bottom button");
                    _UIButton = _bottom_buttons[0];
                    locationInUI = _bottom_buttons;
                }
                else
                {
                    Debug.Log("Going either up or down left/right buttons");
                    int newIndex = button_index - (1 * Math.Sign(y));
                    Debug.Log("New index: " + newIndex);
                    _UIButton = locationInUI[newIndex];
                }
                    
            }
            else
            {
                Debug.Log("At bottom button, going to either top or bottom of left buttons");
                _UIButton = _left_buttons[y > 0 ? _left_buttons.Length - 1 : 0];
                locationInUI = _left_buttons;
            }
        }
        else
        {
            Debug.Log("Not y touch: " + y);
        }
        if (Math.Abs(x) > touchLimit)
        {
            if (locationInUI != _bottom_buttons && locationInUI == _left_buttons)
            {
                Debug.Log("At left buttons, going to right buttons");
                _UIButton = _right_buttons[button_index];
            }
            else if (locationInUI != _bottom_buttons)
            {
                Debug.Log("At right buttons, going to left buttons");
                _UIButton = _left_buttons[button_index];
            }
        }
        else
        {
            Debug.Log("Not x touch: " + x);
        }
        HighlightButton(_UIButton);
        
        

    }

    private void OnEnable()
    {
        string selectionName = "";
        if(_selection != null)
        {
            selectionName = _selection.name;
            Debug.Log(selectionName);
        }
        _selection_name.GetComponent<Text>().text = "Selected: \n" + selectionName;
        _tool_name.GetComponent<Text>().text = "Tool: \n" + _tool;
        //_reset_button.GetComponentInChildren<Text>().text = "Reset: " + selectionName;
        //_delete_button.GetComponentInChildren<Text>().text = "Delete: " + selectionName;
        //_duplicate_button.GetComponentInChildren<Text>().text = "Duplicate: " + selectionName;
    }
    void OnButtonUp(byte controller_id, MLInputControllerButton button)
    { 
        //Debug.Log("Hello");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
