﻿/*Unity 3D Codes for "Building Tilt Brush from Scratch" YouTube tutorial by Fuseman
https://www.youtube.com/watch?v=eMJATZI0A7c

He also uses code from Bartek Drozdz so I feel I should mention that (as I don't know who did what, I just transcribed the code!)
http://www.everyday3d.com/blog/index.php/2010/03/15/3-ways-to-draw-3d-lines-in-unity3d/
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using Photon.Pun;

public class DrawControl : MonoBehaviourPun, ControlMode
{
    private GameObject _pointer;
    //private GameObject _laser;
    //private GameObject _selection;
    //private Selector _selector;
    //private MLInputController _controller;

    [SerializeField]
    private Material lMat = null;

    //public SteamVR_TrackedObject trackedObj;
    [SerializeField]
    private GameObject trackedObj = null;

    //private LineRenderer currLine;
    private MeshLineRenderer currLine;

    [SerializeField]
    private GameObject _pencil = null;

    private int numClicks = 0;

    public void Start()
    {
        _pencil.SetActive(false);
    }

    public void Bumper()
    {
        
    }

    public void BumperHold()
    {
        
    }

    public void ButtonUp()
    {
        
    }

    public void Dragging(int trigger_time)
    {
        //currLine.SetVertexCount (numClicks + 1);
        //currLine.SetPosition (numClicks, trackedObj.transform.position);
        Debug.Log(currLine);
        currLine.AddPoint(trackedObj.transform.position);
        //Debug.Log(RpcTarget.OthersBuffered);
        //Debug.Log(trackedObj.GetPhotonView().ViewID);
        photonView.RPC("RPC_dragging", RpcTarget.OthersBuffered, trackedObj.GetPhotonView().ViewID);
        numClicks++;
    }

    public void Grab()
    {
        GameObject go = new GameObject();
        //currLine = go.AddComponent<LineRenderer> ();
        go.AddComponent<MeshFilter>();
        go.AddComponent<MeshRenderer>();
        currLine = go.AddComponent<MeshLineRenderer>();

        currLine.lmat = lMat;

        //currLine.SetWidth (.1f, .1f);
        currLine.SetWidth(.01f);
        numClicks = 0;

        photonView.RPC("RPC_grab", RpcTarget.OthersBuffered);
    }


    [PunRPC]
    void RPC_grab()
    {
        GameObject go = new GameObject();
        //currLine = go.AddComponent<LineRenderer> ();
        go.AddComponent<MeshFilter>();
        go.AddComponent<MeshRenderer>();
        currLine = go.AddComponent<MeshLineRenderer>();

        currLine.lmat = lMat;

        //currLine.SetWidth (.1f, .1f);
        currLine.SetWidth(.01f);
        numClicks = 0;
    }

    [PunRPC]
    void RPC_dragging(int pointer_id)
    {
        currLine.GetComponent<MeshLineRenderer>().AddPoint(PhotonView.Find(pointer_id).transform.position);
        numClicks++;
    }

    public void Hide(bool condition)
    {
        _pencil.SetActive(!condition);
        _pointer.SetActive(!condition);
    }

    public void Init(GameObject pointer, GameObject laser, Selector selector, GizmoConstraints gizmo, MLInputController controller)
    {
        _pointer = pointer;
    }

    public void LetGo()
    {
        
    }

    public void SetSelection(GameObject selection)
    {
        
    }

    public void Touch(float x, float y, double angle)
    {
        
    }

    public void TouchOff(float x, float y, double angle)
    {
        
    }

    public void TouchOn(float x, float y, double angle)
    {
        
    }

    public string GetName()
    {
        return "Draw Tool";
    }
}