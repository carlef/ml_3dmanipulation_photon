﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoMenuController : MonoBehaviour
{

    [SerializeField]
    private  GameObject _scale = null;
    [SerializeField]
    private GameObject _rotate = null;
    [SerializeField]
    private GameObject _translate = null;
    [SerializeField]
    private GameObject _x = null;
    [SerializeField]
    private GameObject _y = null;
    [SerializeField]
    private GameObject _z = null;
    [SerializeField]
    private GameObject _xyz = null;

    GameObject[] _buttons;

    private void Awake()
    {
        _buttons = new GameObject[] { _x, _y, _z, _xyz, _translate, _rotate, _scale, };
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public int GetButtonsLength()
    {
        return _buttons.Length;
    }

    public void Highlight(int index)
    {
        HighlightReset();   
        _buttons[index].transform.localScale = new Vector3(1.5f, 1, 1.5f);
    }

    public void HighlightReset()
    {
        foreach (GameObject button in _buttons)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void Hide(bool condition)
    {
        foreach(GameObject button in _buttons)
        {
            button.SetActive(!condition);
        }
    }
}
