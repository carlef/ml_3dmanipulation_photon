﻿using UnityEngine;
using UnityEngine.EventSystems;

using System.Collections;
using Photon.Pun;
using UnityEngine.UI;

using UnityEngine.XR.MagicLeap;
using System;

namespace com.defaultcompany.photontest
{
    /// <summary>
    /// Controls manager.
    /// Handles controller input.
    /// </summary>
    public class ControlsManager : MonoBehaviourPunCallbacks, IPunObservable
    {
        #region Private Fields

        private int _trigger_time = 0;
        private int _bumper_threshold = 10;
        private int _bumper_time = 0;
        private int _controlmode_index;
        private int _button_index;

        private const float _rotationSpeed = 30.0f;
        private const float _distance = 2.0f;
        private const float _moveSpeed = 1.2f;
        private float _touchY = 0;
        private float _touchX = 0;
        private double _touchAngle = 0;

        private const float _triggerThreshold = 0.2f;
        private bool _trigger = false;
        private bool _trigger_prev = false; 
        private bool _home_menu_active = false;
        private bool _bumper = false;
        private bool _touch = false;
        private bool _touch_prev = false;
        private bool _dragging = false;
        
        [SerializeField]
        private GameMenu _home_menu = null;
        [SerializeField]
        private GameObject _pointer = null;
        [SerializeField]
        private GameObject _laser = null;
        [SerializeField]
        private GizmoConstraints _gizmo = null;
        [SerializeField]
        private GameObject _menu_navigator = null;
        [SerializeField]
        private Camera _main_camera = null;
        private GameObject _selection;

        private ControlMode _controlMode;
        private ControlMode[] _controlModes;
        private Selector _selector;
        private MLInputController _controller;

        

        #endregion

        #region Public Fields 

        //[Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
        //public static GameObject LocalPlayerInstance;

        #endregion

        #region IPunObservable implementation


        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // We own this player: send the others our data
                //stream.SendNext(_bumper);
                //stream.SendNext(_bumper_time);
            }
            else
            {
                // Network player, receive data
                //_bumper = (bool)stream.ReceiveNext();
                //_bumper_time = (int)stream.ReceiveNext();
            }
        }


        #endregion

        #region MonoBehaviour CallBacks

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>

        public void Start()
        {
            if (!photonView.IsMine)
            {
                _laser.SetActive(false);
                _home_menu.SetActive(false);
                _menu_navigator.SetActive(false);
                
                return;
            }

            //_button_index = 0;
            //_buttons = _home_menu.transform.GetChild(0).GetComponentsInChildren<Button>();
            //UIButton = _buttons[_button_index];
            
            _controlModes = new ControlMode[] { GetComponent<GizmoControl>(), GetComponent<GrabControl>(), GetComponent<DrawControl>() };

            _controlmode_index = 1;
            //SetControlMode(_controlmode_index);
            _controlMode = _controlModes[_controlmode_index];
            _selector = _pointer.GetComponent<Selector>();

            MLInput.Start();
            MLInput.OnControllerButtonDown += OnButtonDown;
            MLInput.OnControllerButtonUp += OnButtonUp;

            _controller = MLInput.GetController(MLInput.Hand.Left);

            _controlModes[0].Init(_pointer, _laser, _selector, _gizmo, _controller);
            _controlModes[1].Init(_pointer, _laser, _selector, _gizmo, _controller);
            _controlModes[2].Init(_pointer, _laser, _selector, _gizmo, _controller);

            _home_menu.SetTool(_controlMode.GetName());

            _home_menu.SetActive(false);
            _menu_navigator.SetActive(false);
            _laser.SetActive(false);
            //_gizmo.Hide(true);
        }

        public override void OnDisable()
        {
            // Always call the base to remove callbacks
            base.OnDisable();
        }

        void OnDestroy()
        {
            if (photonView.IsMine == false && PhotonNetwork.IsConnected)
            {
                return;
            }

            MLInput.OnControllerButtonDown -= OnButtonDown;
            MLInput.OnControllerButtonUp -= OnButtonUp;
            MLInput.Stop();
        }

        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity on every frame.
        /// </summary>
        public void Update()
        {
            // we only process Inputs if we are the local player
            if (photonView.IsMine)
            {
                ProcessInputs();
                ControllerActions();
                MovePointer();
            }
        }

        #endregion

        #region Custom

        #if !UNITY_5_4_OR_NEWER
        /// <summary>See CalledOnLevelWasLoaded. Outdated in Unity 5.4.</summary>
        void OnLevelWasLoaded(int level)
        {
            this.CalledOnLevelWasLoaded(level);
        }
        #endif

        /// <summary>
        /// Processes the inputs. Maintain a flag representing when the user is pressing Fire.
        /// 

        void ControllerActions()
        {
            //trigger let go
            if (_trigger_prev && !_trigger)
            {
                if (_home_menu_active)
                {
                    UiActivate(true);
                }
                else
                {
                    _trigger_time = 0;
                    _dragging = false;

                    if(_selection != null)
                    {
                        _controlMode.LetGo();
                    }
                }
            }

            //trigger pushed down
            if (!_trigger_prev && _trigger)
            {
                if (!_home_menu_active)
                {
                    if (_selection != null || _controlMode is DrawControl)
                    {
                        //Debug.Log("Grabbing");
                        _controlMode.Grab();
                    }

                    _dragging = true;
                }
            }

            // Bumper held down
            if (_bumper_time > _bumper_threshold)
            {
                _laser.SetActive(true);
                _selector = _laser.GetComponent<Selector>();
            }

            // touch off
            if (_touch_prev && !_touch)
            {
                if (_home_menu_active)
                {
                    _home_menu.TouchOff(_touchX, _touchY);
                }
                else
                {
                    //_gizmo_menu.SetActive(false);
                    _controlMode.TouchOff(_touchX, _touchY, _touchAngle);
                }
            }

            // touch on 
            if(!_touch_prev && _touch)
            {
                if (!_home_menu_active)
                {
                    _controlMode.TouchOn(_touchX, _touchY, _touchAngle);
                    //_gizmo_menu.SetActive(true);
                }
            }

            if (_dragging)
            {
                //Debug.Log("Dragging");
                _trigger_time += 1;
                _controlMode.Dragging(_trigger_time);
            }
        }

        void ProcessInputs()
        {
            if (_controller.TriggerValue > 0.2f)
            {
                _trigger_prev = _trigger;
                _trigger = true;
            }
            else
            {
                _trigger_prev = _trigger;
                _trigger = false;
            }
            if (_bumper)
            {
               _bumper_time += 1;
            }
            if (_controller.Touch1PosAndForce.z > 0.01f)
            {
                _touch_prev = _touch;
                _touch = true;
                _touchX = _controller.Touch1PosAndForce.x;
                _touchY = _controller.Touch1PosAndForce.y;
                _touchAngle = Math.Atan2(_touchX, _touchY)*180/Math.PI;
                if (!_home_menu_active)
                {
                    _controlMode.Touch(_touchX, _touchY, _touchAngle);
                }
            }
            if (_controller.Touch1PosAndForce.z < 0.01f)
            {
                _touch_prev = _touch;
                _touch = false;
            }
        }

        void MovePointer()
        {
            //Debug.Log(_controller.Position);
            transform.position = _controller.Position;
            transform.rotation = _controller.Orientation;
            if (_home_menu_active 
                && _pointer.GetComponent<Selector>().GetSelected() != null 
                && _pointer.GetComponent<Selector>().GetSelected().GetComponent<UnityEngine.UI.Button>() != null 
                && !_laser.activeSelf)
            {
                _home_menu.HighlightButton(_pointer.GetComponent<Selector>().GetSelected().GetComponent<UnityEngine.UI.Button>());
            }
        }

        void OnButtonDown(byte controller_id, MLInputControllerButton button)
        {
            if ((button == MLInputControllerButton.Bumper))
            {
                _bumper = true;
            }
        }

        void OnButtonUp(byte controller_id, MLInputControllerButton button)
        {
            if (button == MLInputControllerButton.HomeTap)
            {
                UiActivate(false);
            }

            if (_bumper)
            {
                if (_home_menu_active)
                {
                    UiActivate(true);
                    if(_selector.GetSelected() != null && _selector.GetSelected().GetComponent<UnityEngine.UI.Button>() == null)
                    {
                        Debug.Log("Menu open, selecting object");
                        SelectObject();
                    }
                }
                else
                {
                    Debug.Log("Menu closed, selecting object");
                    SelectObject();
                    //if (_bumper_time < _bumper_threshold)
                    //{
                    //_controlMode.Bumper();

                    //}
                    //_debug.ScreenLog(_gizmo.GetGizmode().ToString());
                    //if (_laser.activeSelf)
                    //{
                    //}
                }
                _selector = _pointer.GetComponent<Selector>();
                _laser.SetActive(false);
            }

            _dragging = false;
            _bumper = false;
            _bumper_time = 0;
            _controlMode.ButtonUp();

        }

        public void NextGizmode()
        {
            Debug.Log("NextGizmode was called");
            _controlmode_index++;
            _controlMode.Hide(true);
            _controlMode = _controlModes[_controlmode_index % _controlModes.Length];
            _controlMode.Hide(false);
        }

        public void SetControlMode(int index)
        {
            foreach(ControlMode c in _controlModes)
            {
                c.Hide(true);
            }
            _controlMode = _controlModes[index];
            _controlMode.Hide(false);
            _home_menu.SetTool(_controlMode.GetName());
        }

        void ActivateUIButton()
        {
            //I want to change the button size when selected but looks like it's more cumbersome than I thought: 
            //Debug.Log(UIButton.GetComponent<RectTransform>().sizeDelta);
            //float _buttonX = UIButton.GetComponent<RectTransform>().sizeDelta.x;
            //UIButton.GetComponent<RectTransform>(). = _buttonX * 1.1f;
            //_buttons[++_button_index % _buttons.Length].GetComponent<RectTransform>().sizeDelta = new Vector2 (_buttonX, _buttonY);
            //_buttons[(--_button_index + _buttons.Length) % _buttons.Length].GetComponent<RectTransform>().sizeDelta = new Vector2 (_buttonX, _buttonY);
        }

        void UiActivate(bool pushButton)
        {
            _home_menu.transform.position = _main_camera.transform.position + _main_camera.transform.forward * 0.65f;
            _home_menu.transform.rotation = _main_camera.transform.rotation * Quaternion.Euler(0, 0, 0);

            _controlMode.Hide(!_home_menu_active);

            //if (_controlMode is GrabControl)
            //{
            //    _pointer.SetActive(_home_menu_active);
            //}
           
            _home_menu_active = !_home_menu_active;
            _home_menu.SetActive(_home_menu_active);
            //_menu_navigator.SetActive(_home_menu_active);

            if(_controlMode is GizmoControl)
            {
                _pointer.SetActive(_home_menu_active);

            }
            else
            {
                _pointer.SetActive(true);

            }

            if (pushButton)
            {
                _home_menu.PushButton();
            }
            //EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
        }

        void SelectObject()
        {
            DeselectObject();
            //GameObject newSelection = _selector.GetSelected();
            //if (newSelection != null)
            //{
                //Debug.Log("Selecting: " + newSelection.name);
            _selection = _selector.GetSelected();
            _home_menu.GetComponent<GameMenu>().SetSelection(_selection);
            _controlModes[0].SetSelection(_selection);
            _controlModes[1].SetSelection(_selection);
            //}

        }

        void DeselectObject()
        {
            if (_selection != null && _selection.GetComponent<SelectableObject>() != null)
            {
                _selection.GetComponent<SelectableObject>().OnDeselect();
                _selection = null;
            }
        }

        #endregion

        #region Private Methods

        #endregion
    }
}