﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformConstraint : MonoBehaviour
{
    private GameObject _target;

    public void AddTranslation(Vector3 _position, Vector3 _original_scale, Quaternion _original_rotation )
    {
        _target.transform.position = transform.position + _position;
        _target.transform.localScale = new Vector3(
            _original_scale.x*transform.localScale.x,
            _original_scale.y*transform.localScale.y, 
            _original_scale.z*transform.localScale.z
            );
        _target.transform.rotation = transform.rotation * _original_rotation;
    }

    public void SetTarget(GameObject target)
    {
        _target = target;
    }
}
