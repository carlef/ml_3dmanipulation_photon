﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gizmode : MonoBehaviour
{
    private List<GameObject> _axes = new List<GameObject>();

    private void Awake()
    {
        foreach (Transform child in transform)
        {
            _axes.Add(child.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
           
    }

    public void SetAxis(int axis)
    {
        if(axis < 3)
        {
            _axes[axis].SetActive(true);
            _axes[(axis + 1) % _axes.Count].SetActive(false);
            _axes[((axis - 1) + _axes.Count) % _axes.Count].SetActive(false);
        }
        else
        {
            _axes[0].SetActive(true);
            _axes[1].SetActive(true);
            _axes[2].SetActive(true);
        }
    }

    public void SetGizmodeActive(bool active, int axis)
    {
        foreach(GameObject child in _axes)
        {
            child.SetActive(false);
        }
        if (active)
        {
            SetAxis(axis);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
