﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SelectableObject : MonoBehaviourPun, IPunOwnershipCallbacks
{
    // Start is called before the first frame update

    private List<Collider> _colliders;
    private MeshFilter[] _meshes;
    //private List<Material> _materials;
    private bool _selected = false;
    private Renderer[] _children;
    //[SerializeField]
    private Shader _outliner;
    private float[] _outline_rgba = new float[4];
    private string _prefab_name = "";

    void Awake()
    {
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        //DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        _outliner = Shader.Find("Interface3/Surface_Outline");
        _meshes = GetComponentsInChildren<MeshFilter>();
        _children = GetComponentsInChildren<Renderer>();
        _outline_rgba[0] = 255f;
        _outline_rgba[0] = 255f;
        _outline_rgba[0] = 255f;
        _outline_rgba[0] = 100f;

        foreach (MeshFilter _mf in _meshes)
        {
            if (_mf && _mf.sharedMesh)
            {
                Bounds bounds = _mf.sharedMesh.bounds;
                BoxCollider collider = gameObject.AddComponent<BoxCollider>();
                collider.center = bounds.center;
                collider.size = bounds.size;
                collider.isTrigger = true;
            }
            //MeshCollider col = gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
            //col.convex = true;
            //col.isTrigger = true;
            //col.sharedMesh = _m.mesh;
        }
        ToggleOutline();
    }

    public void OnSelect()
    {
        photonView.RequestOwnership();
        _selected = true;
        ToggleOutline();
    }

    public void OnDeselect()
    {
        _selected = false;
        ToggleOutline();
    }

    public void SetPrefabName(string name)
    {
        _prefab_name = name;
    }

    public string GetPrefabName()
    {
        return _prefab_name;
    }

    public void ToggleOutline()
    {
        foreach (Renderer rend in _children)
        {
            foreach (Material mat in rend.materials)
            {
                if (_selected)
                {
                    Texture _diffuse = mat.mainTexture;
                    mat.shader = _outliner;
                    mat.SetTexture("_MainTex", _diffuse);
                    mat.SetFloat("_OutlineThickness", 0.005f);
                    //mat.SetFloatArray("_Color", _outline_rgba);
                    mat.SetColor("_OutlineColor", Color.white);
                    mat.SetFloat("_Smoothness", 0f);
                }
                else
                {
                    mat.shader = Shader.Find("Standard");
                }
            }
        }
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        throw new System.NotImplementedException();
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        throw new System.NotImplementedException();
    }
}
