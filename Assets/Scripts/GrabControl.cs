﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class GrabControl : MonoBehaviourPun, ControlMode
{

    private GameObject _pointer;
    private GameObject _laser;
    private Selector _selector;
    private GameObject _selection;
    private Transform _selection_parent;
    private MLInputController _controller;

    //private debug _debug;

    // Start is called before the first frame update
    public void Start()
    {
        //_debug = GameObject.Find("Debug").GetComponent<debug>();
    }

    public void BumperHold()
    {
        
    }

    public void Dragging(int trigger_time)
    {

    }

    public void Grab()
    {
        _selection_parent = _selection.transform.parent;
        _selection.transform.parent = transform;
        photonView.RPC("RPC_grab", RpcTarget.OthersBuffered, photonView.ViewID, _selection.GetPhotonView().ViewID);
    }

    [PunRPC]
    void RPC_grab(int parent_id, int selection_id)
    {
        Transform parent_transform = PhotonView.Find(parent_id).transform;
        PhotonView.Find(selection_id).transform.parent = parent_transform;
    }

    [PunRPC]
    void RPC_let_go(int selection_id)
    {
        PhotonView.Find(selection_id).transform.parent = null;
    }

    public void Init(GameObject pointer, GameObject laser, Selector selector, GizmoConstraints gizmo, MLInputController controller)
    {
        _pointer = pointer;
        _laser = laser;
        _selector = selector;
        _controller = controller;
    }

    public void LetGo()
    {
        //_selection.transform.parent = _selection_parent;
        _selection.transform.parent = null;
        //if(_selection_parent.gameObject.GetPhotonView() != null)
        
        {
            Debug.Log("fuck");
            //photonView.RPC("RPC_grab", RpcTarget.OthersBuffered, _selection_parent.gameObject.GetPhotonView().ViewID, _selection.GetPhotonView().ViewID);
            photonView.RPC("RPC_let_go", RpcTarget.OthersBuffered, _selection.GetPhotonView().ViewID);
        }
        //Debug.Log("Selection parent: " + _selection_parent);
        //Debug.Log("OthersBuffered: " + RpcTarget.OthersBuffered);
        //Debug.Log("Selection parent ID: " + _selection_parent.gameObject.GetPhotonView().ViewID);
        //Debug.Log("Selection ID: " + _selection.GetPhotonView().ViewID);
        
    }

    public void SetSelection(GameObject selection)
    {
        _selection = selection;
    }

    public void Bumper()
    {
        //let go of target?
    }

    public void Touch(float x, float y, double angle)
    {

        if (_selection != null && _controller.TriggerValue > 0.02f)
        {
            //Vector3 _direction = Vector3.Normalize(_selection.transform.position - _controller.Position);
            //_selection.transform.position += _direction * Time.deltaTime * y; 

            float newScale = y * 0.01f;
            _selection.transform.localScale += new Vector3(newScale, newScale, newScale);
        }
    }

    public void TouchOn(float x, float y, double angle)
    {

    }

    public void TouchOff(float x, float y, double angle)
    {

    }

    public void Hide(bool condition)
    {
        _pointer.SetActive(!condition);
    }

    public void ButtonUp()
    {

    }

    public string GetName()
    {
        return "Grab Tool";
    }
}
