﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public interface ControlMode
{
    void Start();
    void Init(GameObject pointer, GameObject laser, Selector selector,
        GizmoConstraints gizmo, MLInputController controller);
    void Grab();
    void LetGo();
    void Dragging(int trigger_time);
    void BumperHold();
    void SetSelection(GameObject selection);
    void Bumper();
    void Touch(float x, float y, double angle);
    void TouchOn(float x, float y, double angle);
    void TouchOff(float x, float y, double angle);
    void ButtonUp();
    void Hide(bool condition);
    string GetName();
}
