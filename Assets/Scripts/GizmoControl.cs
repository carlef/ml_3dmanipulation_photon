﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using Photon.Pun;
using System;

public class GizmoControl : MonoBehaviourPunCallbacks, ControlMode
{
    private GameObject _pointer;
    private GameObject _laser;
    private GameObject _selection;
    private Selector _selector;
    private GizmoConstraints _gizmo;
    private MLInputController _controller;
    private Quaternion _gizmo_rotation;
    private Quaternion _gizmo_child_rotation;
    private Vector3 _controller_position;
    private Quaternion _controller_rotation;
    private Vector3 _gizmo_position;
    private Vector3 _gizmo_child_offset;
    //private Vector3 _gizmo_child_rotation;
    private Vector3 _gizmo_child_scale;
    private Vector3 _gizmo_controller_offset;
    private Vector3 _gizmo_menu_scale;
    private Vector3 _gizmo_menu_small = new Vector3(0.02f, 0.02f, 0.02f);
    private string[] _axis_names;
    private bool _dragging;
    private int _axis;

    [SerializeField]
    private GizmoMenuController _gizmo_menu = null;

    //private debug _debug;



    // Start is called before the first frame update
    public void Start()
    {
        //_debug = GameObject.Find("Debug").GetComponent<debug>();
        _axis = 0;
        _dragging = false;
        _axis_names = new string[] {"x_axis", "y_axis", "z_axis", "all_axis" };
        _gizmo_menu.Hide(true);
        _gizmo_menu_scale = _gizmo_menu.transform.localScale;
        _gizmo_menu.transform.localScale = _gizmo_menu_small;
        //_gizmo.Hide(true);
    }

    public void Init(GameObject pointer, GameObject laser, Selector selector, 
        GizmoConstraints gizmo, MLInputController controller)
    {
        _pointer = pointer;
        _laser = laser;
        _selector = selector;
        _gizmo = gizmo;
        _controller = controller;

        if (!photonView.IsMine)
        {
            gizmo.Hide(true);
        }
    }

    public void Grab()
    {
        _gizmo_position = _gizmo.transform.position;
        _gizmo_child_offset = _selection.transform.position - _gizmo_position;
        _gizmo_child_rotation = _selection.transform.rotation; // eulerAngles;
        _gizmo_child_scale = _selection.transform.localScale;
        _gizmo_controller_offset = _gizmo_position - _controller.Position;
        _gizmo_rotation = _gizmo.transform.rotation;
        _controller_rotation = _controller.Orientation;
        _controller_position = _controller.Position;
        _dragging = true;
    }

    public void LetGo()
    {
        _gizmo.Release(new Vector3 (0,0,0));
        _dragging = false;
    }

    public void Dragging(int trigger_time)
    {
        if (_selection != null && !_laser.activeSelf)
        {
            _gizmo.GizmoTransform(_pointer.transform, _gizmo_controller_offset, _controller_position, _gizmo_rotation, _controller_rotation);
            _gizmo.GetComponent<TransformConstraint>().AddTranslation(_gizmo_child_offset, _gizmo_child_scale, _gizmo_child_rotation);
        }
    }

    public void BumperHold()
    {
    }

    public void SetSelection(GameObject selection)
    {
        _gizmo.GetComponent<TransformConstraint>().SetTarget(selection);
        _selection = selection;
        //_debug.ScreenLog("New selection: " + selection);

    }

    public void Bumper()
    {
        //_gizmo.NextGizmode();
    }

    public void Touch(float x, float y, double angle)
    {
        int currentButton = 0;
        if (angle < 0)
        {
            if (angle > -45)
            {
                _axis = 3;
                currentButton = 3;
            }
            else if (angle > -90)
            {
                _axis = 0;
                currentButton = 0;
            }
            else if (angle > -135)
            {
                _axis = 1;
                currentButton = 1;
            }
            else
            {
                _axis = 2;
                currentButton = 2;
            }
            _gizmo.SetGizmoAxis(_axis);
        }
        else
        {
            if (angle < 60)
            {
                _gizmo.SetGizmode(0);
                currentButton = 4;
            }
            else if (angle < 120)
            {
                _gizmo.SetGizmode(2);
                currentButton = 5;
            }
            else if (angle < 180)
            {
                _gizmo.SetGizmode(1);
                currentButton = 6;
            }
        }
        _gizmo_menu.Highlight(currentButton);
    }

    public void TouchOn(float x, float y, double angle)
    {
        //_gizmo_menu.GetComponent<GizmoMenuController>().Hide(false);
        _gizmo_menu.transform.localScale = _gizmo_menu_scale;
    }

    public void TouchOff(float x, float y, double angle)
    {
        //if (x < 0)
        //{
        //    _axis = (++_axis) % (_axis_names.Length);
        //    //Debug.Log("GizmoControl set axis to: " + _axis);
        //    _gizmo.SetGizmoAxis(_axis);
        //}
        //else if (x > 0)
        //{
        //    _gizmo.NextGizmode();
        //}
        _gizmo_menu.HighlightReset();
        _gizmo_menu.transform.localScale = _gizmo_menu_small;
        //Debug.Log(angle);
    }

    public void ButtonUp()
    {
        
    }

    public void Hide(bool condition)
    {
        _gizmo.Hide(condition);
        _gizmo_menu.Hide(condition);
    }

    public void Update()
    {
        if (photonView.IsMine)
        {
            if (!_dragging)
            {
                _gizmo.UpdatePosition(new Vector3(_controller.Position.x, _controller.Position.y + 0.1f, _controller.Position.z));
            }
        }
    }

    public string GetName()
    {
        return "Gizmo Tool";
    }

}
