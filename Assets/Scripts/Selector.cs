﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;

public class Selector : MonoBehaviourPunCallbacks
{
    private GizmoConstraints _gizmo;
    private List<Collider> _colliders = new List<Collider>();
    private bool _gizmo_time;
    private bool _lock;
    [SerializeField]
    private GameMenu _gameMenu = null;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (photonView.IsMine)
        {
            //Debug.Log("collison enter: " + other);
            _colliders.Add(other);
            if (other.gameObject.GetComponent<UnityEngine.UI.Button>() != null)
            {
                _gameMenu.HighlightButton(other.gameObject.GetComponent<UnityEngine.UI.Button>());

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Debug.Log("collison exit: " + other);
        if (photonView.IsMine)
        {
            _colliders.Remove(other);
        }
    }

    public List<Collider> GetColliders()
    {
            return _colliders;
    }

    public void Delete(GameObject other)
    {
        if(other.GetComponents<Collider>() != null)
        {
            Collider[] other_colliders = other.GetComponents<Collider>();
            foreach (Collider col in other_colliders)
            {
                if (_colliders.Contains(col))
                {
                    _colliders.Remove(col);
                }

            }
        }
        
    }

    public GameObject GetSelected()
    {
        List<Collider> cols = GetColliders();
        if (cols.Count > 0)
        {
            for (int i = cols.Count - 1; i >= 0; i--)
            {
                GameObject obj = cols[i].gameObject;
                if (obj.GetComponent<UnityEngine.UI.Button>() != null)
                {
                    return obj;
                }
                if (obj.GetComponent<SelectableObject>() != null)
                {
                    obj.GetComponent<SelectableObject>().OnSelect();
                    _gameMenu.SetSelection(obj);
                    return obj;
                }
            }
            //%Debug.Log("Not selectable");
        }
        return null;
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
