﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

using UnityEngine.XR.MagicLeap;

public class GizmoConstraints : MonoBehaviourPunCallbacks
{
    private List<Gizmode> _gizmos = new List<Gizmode>();
    //private bool _active;
    private int _gizmode;
    private int _axis;

    void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        if (photonView.IsMine)
        {
            
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        //DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine)
        {
            _gizmode = 0;
            _axis = 0;
            foreach (Transform child in transform)
            {
                //Debug.Log(child.gameObject);
                _gizmos.Add(child.gameObject.GetComponent<Gizmode>());
            }
            //_gizmos = new GameObject[] { GameObject.Find("gizmo_Translate"), GameObject.Find("gizmo_Scale"), GameObject.Find("gizmo_Rotate")};
            Hide(true);
            //Debug.Log(_gizmos.Count);
        }
    }

    public void NextGizmode()
    {
        //if (_active)
        {
            _gizmode++;
            _gizmos[_gizmode % _gizmos.Count].SetGizmodeActive(true, _axis);
            _gizmos[(_gizmode + 1) % _gizmos.Count].SetGizmodeActive(false, _axis);
            _gizmos[((_gizmode - 1) + _gizmos.Count) % _gizmos.Count].SetGizmodeActive(false, _axis);
            //Debug.Log(_gizmos[_gizmode % _gizmos.Length]);
        }

    }

    public void SetGizmode(int mode)
    {
        _gizmode = mode;
        _gizmos[_gizmode % _gizmos.Count].SetGizmodeActive(true, _axis);
        _gizmos[(_gizmode + 1) % _gizmos.Count].SetGizmodeActive(false, _axis);
        _gizmos[((_gizmode - 1) + _gizmos.Count) % _gizmos.Count].SetGizmodeActive(false, _axis);
    }

    public int GetGizmode()
    {
        //Debug.Log(_gizmos.Count);
        return _gizmode % _gizmos.Count;
    }

    public void GizmoTransform(Transform controllerPos, Vector3 offset, Vector3 controllerOriginalPos, Quaternion rotation, Quaternion controllerRotation)
    {
        switch (GetGizmode())
        {
            case 0:
                //translating
                Vector3 newPos = transform.position;
                switch (GetGizmoAxis())
                {
                    case 0:
                        newPos.x = controllerPos.transform.position.x + offset.x;
                        break;
                    case 1:
                        newPos.y = controllerPos.transform.position.y + offset.y;
                        break;
                    case 2:
                        newPos.z = controllerPos.transform.position.z + offset.z;
                        break;
                    default:
                        newPos = controllerPos.transform.position + offset;
                        break;
                }
                UpdatePosition(newPos);
                break;
            case 1:
                //scaling
                float scaleFac = (controllerOriginalPos - controllerPos.position + offset).magnitude / offset.magnitude;
                Vector3 newScale;
                switch (GetGizmoAxis())
                {
                    case 0:
                        newScale = new Vector3(scaleFac, 1, 1);
                        break;
                    case 1:
                        newScale = new Vector3(1, scaleFac, 1);
                        break;
                    case 2:
                        newScale = new Vector3(1, 1, scaleFac);
                        break;
                    default:
                        newScale = new Vector3(scaleFac, scaleFac, scaleFac);
                        break;
                }
                transform.localScale = newScale;
                break;
            case 2:
                //rotating
                Quaternion newRot = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
                Quaternion offsetRot = controllerPos.rotation * Quaternion.Inverse(controllerRotation);
                //float rotateFac = (controllerOriginalPos - controllerPos.position).magnitude * 5;
                switch (GetGizmoAxis())
                {
                    case 0:
                        //newRot.x += rotateFac;
                        newRot.x = offsetRot.x;
                        break;
                    case 1:
                        //newRot.y += rotateFac;
                        newRot.y = offsetRot.y;
                        break;
                    case 2:
                        //newRot.z += rotateFac;
                        newRot.z = offsetRot.z;
                        break;
                    default:
                        //newRot = Quaternion.LookRotation((controllerPos.position - transform.position).normalized);
                        newRot = offsetRot;
                        break;
                }
                UpdateRotation(newRot);
                break;
            default:
                Debug.Log("Gizmode out of bounds");
                break;
        }
    }

    public void SetGizmoAxis(int axis)
    {
        //Debug.Log("SetGizmoAxis received: " + axis);
        _axis = axis;
        _gizmos[GetGizmode()].SetAxis(_axis);
    }

    public int GetGizmoAxis()
    {
        return _axis;
    }

    public void Hide(bool boolean)
    {
        if (!boolean)
        {
            NextGizmode();
            NextGizmode();
            NextGizmode();
        }
        else
        {
            _gizmos[0].SetGizmodeActive(false, _axis);
            _gizmos[1].SetGizmodeActive(false, _axis);
            _gizmos[2].SetGizmodeActive(false, _axis);
            _gizmode = 0;
        }
    }

    public void UpdatePosition(Vector3 position)
    {
        transform.position = position;
    }

    public void UpdateRotation(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    public void Release(Vector3 resetPosition)
    {
        transform.rotation = new Quaternion (0, 0, 0, 0);
        transform.localScale = new Vector3 (1, 1, 1);
        transform.position = resetPosition;
    }

    public Transform GetGizmoTransform()
    {
        return transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
